import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface FirstInterface {
    private List<Integer> createList() {
        List<Integer> myList = List.of(-1, -2, 0, 4, 5);

        return myList;
    }

    default void printElement() {
        List<Integer> myList = createList();

        int number = myList.stream()
                .filter(x -> x > 0)
                .findFirst()
                .get();

        System.out.println("First element grater than zero: " + number);
    }
}
