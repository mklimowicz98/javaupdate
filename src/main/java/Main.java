import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void zadanie1(){
        FirstClass firstClass = new FirstClass();
        firstClass.printElement();
    }

    public static void zadanie2() {
        var mySecondList = List.of(1, -4, 1, 0);
        var unmodifiableList = Collections.unmodifiableList(mySecondList);
        var elements = unmodifiableList.stream()
                .filter(x -> x > 0)
                .collect(Collectors.toList());

        System.out.println("Elements from list grater than zero: " + elements);

        //lepiej nie dodawać bo będzie błąd
        //unmodifiableList.add(6);
    }

    public static void zadanie3() {
        String s1 = "";
        System.out.println("isBlank: " + s1.isBlank());
        System.out.println("isEmpty: " + s1.isEmpty());

        String s2 = "AB\nAB\nAB";
        long numberOfLines = s2.lines()
                .count();
        System.out.println("Number of lines: " + numberOfLines);

        String s3 = " DB ";
        System.out.println(s3.strip()); //kasuje spacje z przodu i tyłu
        System.out.println(s3.stripLeading()); //kasuje tylko z przodu
        System.out.println(s3.stripTrailing()); //kasuje tylko z tyłu

        String s4 = "==";
        System.out.println("repeat: " + s4.repeat(5)); //powiela
    }

    public static void zadanie4() {
        try {
            long resultIdentical = Files.mismatch(Path.of("dokument1.txt"), Path.of("dokument2.txt"));
            System.out.println(resultIdentical); //daje -1 czyli są identyczne

            long resultDifferent = Files.mismatch(Path.of("dokument1.txt"), Path.of("dokument3.txt"));
            System.out.println(resultDifferent); //zwraca numer kolumny w którym zaczyna się różnić
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            System.out.println(Files.readString(Path.of("dokument1.txt")));
            System.out.println(Files.readString(Path.of("dokument2.txt")));
            System.out.println(Files.readString(Path.of("dokument3.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        zadanie1();

        zadanie2();

        zadanie3();

        zadanie4();

    }
}
